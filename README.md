
= KITCHEN MANAGER DDD =

---

NOTE: I recommend to open this document into browser outside Github - some of text editor can cause problem with proper displaying ASCII schemas!!

---

= REQUIREMENTS PART =

**Use cases:**

- As a user I can calculate daily take depend on sold dishes



= MODELING SYSTEM PART =

---

"Na Oko" DDD design: I think we need some Dish aggregate and that Dish should be builded upon some Ingredients(List of ingredients used into Dish in that case as value objects) and as well we need store/search information about Dishes somewhere so we also need some Repositories and as well I can say that we gonna have Domain event (DishCooked) - when the dish is ready and can be tooked of from kitchen in order to inform waiter about it, we can add additional Domain event DishCookingAccepted which gonna inform waiter that cooking of that dish has been accepted (as long as we have enough ingredients etc.).

== Strategic Modeling: ==

As long as this is "Na Oko" strategic modeling we don't do any modeling 

---

===== Context Mapping (Integration beetween contexts) =====

As long as this is "Na Oko" strategic modeling we don't have any Integration beetween contexts

===== Architecture of application =====

**In our system we gonna use hexagonal architecture (sometimes called port - adapter architecture).**
- PORT -> most often interface
- ADAPTER -> implementation of that interface

NOTE: The idea of Hexagonal architecture is to isolate central logic (bussines model) from outside of system, which cause that we gonna increase portability of our application and can quickly be adapted for new infrastructure usage without touching bussines model. 

//More about DDD + Hexagonal architecture: //
[[https://vaadin.com/learn/tutorials/ddd/ddd_and_hexagonal | Hexagonal architecture]]
 

```

                                     +-----------------------------------------------------------------------------------------+
                                     |                                                                                         |
                                     |            Hexagonal architecture (as square view) for domain application core          |
                                     |                                                                                         |
                                     +-----------------------------------------------------------------------------------------+






                                     +-----------------------------------------------------------------------------------------+
                                     |                                                                                         |
                                     |                                      Adapters                                           |
                                     |                                                                                         |
                                     |        +----------------------------------------------------------------------+         |
                                     |        |                                                                      |     +-----------------------> MongoDB/SQL/etc..
                                     |        |                         Application layer                            |         |
                                     |        |                              (PORTS)                                 |         |
                                     |        |       +----------------------------------------------------+         |         |
                                     |        |       |                                                    |         |         |
                                     |        |       |                                                    |         |         |
                                     |        |       |                                                    |         |         |
                                     |        |       |                                                    |         |         |
                                     |        |       |                                                    |         |         |
                                     |        |       |                                                    |         |         |
                                     |        |       |                    Domain Layer                    |         |         |
SOAP/REST/CLI/etc.. <--------------------+    |       |                                                    |         |     +-----------------------> HTTP/FTP/SMTP/etc...
                                     |        |       |                                                    |         |         |
                                     |        |       |                      (PORTS)                       |         |         |
                                     |        |       |                                                    |         |         |
                                     |        |       |                                                    |         |         |
                                     |        |       |                                                    |         |         |
                                     |        |       |                                                    |         |         |
                                     |        |       |                                                    |         |         |
                                     |        |       |                                                    |         |         |
                                     |        |       |                                                    |         |         |
                                     |        |       |                                                    |         |         |
                                     |        |       +----------------------------------------------------+         |         |
                                     |        |                                                                      |     +-----------------------> Kafka/SQS/etc...
                                     |        |                                                                      |         |
                                     |        +----------------------------------------------------------------------+         |
                                     |                                                                                         |
                                     |                                                                                         |
                                     |                                                                                         |
                                     +-----------------------------------------------------------------------------------------+

```

---

== Tactical Modeling ==

**Quick description:**
- Aggregate, Entity, Value Object (Entity, Value Object types must remain transactionally consistent throughout the Aggregate’s lifetime)
- Repository (Aggregate is persisted using repository and later searched for within and retrieved from it)
- Services (stateless operations to perform business logic that don’t fit naturally as an operation on an Entity or a Value Object)
 - Domain Service - carry out domain-specific operations, which may involve multiple domain objects
 - Application Service - carry out application-specific operations transactions and event handling (in case of CQRS also Commands can be defined here)
- Domain Events (to indicate the occurrence of significant happenings in the domain. Domain Events can be modeled on a few different ways. When they capture occurrences that are a result of some Aggregate 			command operation, the Aggregate itself publishes the Event) and can be published from domain servcie in our case we gonna depend on Domain event discovered during Event Storming session.


**Used nomenclature:**
- << aggregate root >>
- << aggregate >>
- << entity >>
- << value object >>
- << domain service >>
- << application service >>
- << domain event >>


Example of building block on designed picture:

```

+----------------------+
|  << nomenclature >>  |
|                      |
|   BuildingBlockName  |
+----------------------+

```













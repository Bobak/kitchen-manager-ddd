/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.axisdata.kitchenmanager;

import java.time.LocalDate;
import net.axisdata.kitchenmanager.application.KitchenAppService;
import net.axisdata.kitchenmanager.domain.model.dish.DishCalculatorService;
import net.axisdata.kitchenmanager.domain.model.dish.DishRepository;
import net.axisdata.kitchenmanager.domain.model.dish.MenuDish;
import net.axisdata.kitchenmanager.port.adapter.inmemory.InMemoryDishRepository;

public class KitchenClient {

    public static void main(String[] args) throws InterruptedException {

        //We could turn on event logging capabilities by uncomment below line this gonna cuase that w gonna have two times captured events by our handlers and in our Event app service
        //new EventAppService();
        DishRepository dishRepo = new InMemoryDishRepository();
        DishCalculatorService dishCalculator = new DishCalculatorService(dishRepo);
        KitchenAppService kitchen = new KitchenAppService(dishRepo);

        //Cooking dishes you can add more dishes here currenlty our menu have only MOSKOL and TORTILLA_DA_PATATAS 
        kitchen.makeDish(MenuDish.MOSKOL);
        kitchen.makeDish(MenuDish.TORTILLA_DA_PATATAS);
        kitchen.makeDish(MenuDish.MOSKOL);
        kitchen.makeDish(MenuDish.MOSKOL);
        kitchen.makeDish(MenuDish.MOSKOL);
        kitchen.makeDish(MenuDish.TORTILLA_DA_PATATAS);

        //Calculator of Daily 
        var calDailyOutput = dishCalculator.calculateDaily(LocalDate.now());
        System.out.println(calDailyOutput);
        var calcDailyPerDishOutput = dishCalculator.calculateDailyPerDish(LocalDate.now(), MenuDish.MOSKOL);
        System.out.println(calcDailyPerDishOutput);

    }

}

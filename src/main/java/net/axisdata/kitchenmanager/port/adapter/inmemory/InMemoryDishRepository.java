/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.axisdata.kitchenmanager.port.adapter.inmemory;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import net.axisdata.kitchenmanager.domain.model.dish.Dish;
import net.axisdata.kitchenmanager.domain.model.dish.DishRepository;
import net.axisdata.kitchenmanager.domain.model.dish.MenuDish;

public class InMemoryDishRepository implements DishRepository {

    private final HashMap<String, Dish> inMemoryStore;

    public InMemoryDishRepository() {
        inMemoryStore = new HashMap();
    }

    @Override
    public void addDish(Dish dish) {
        inMemoryStore.put(dish.getId(), dish);
    }

    @Override
    public void updateDish(Dish dish) {
        inMemoryStore.replace(dish.getId(), dish);
    }

    @Override
    public Dish getDish(String dishId) {
        return inMemoryStore.get(dishId);
    }

    @Override
    public List<Dish> getDishesByDay(LocalDate day) {
        return inMemoryStore.values().stream().filter(x -> x.getMakedAt().equals(day)).collect(Collectors.toList());
    }

    @Override
    public List<Dish> getDishesByDayAndDish(LocalDate day, MenuDish dish) {
        return inMemoryStore.values().stream().filter(x -> x.getMakedAt().equals(day) && x.getDish() == dish).collect(Collectors.toList());
    }

    @Override
    public String nextIdentity() {
        return UUID.randomUUID().toString().toUpperCase();
    }

}

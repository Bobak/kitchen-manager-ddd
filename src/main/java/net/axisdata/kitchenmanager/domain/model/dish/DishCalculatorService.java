/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.axisdata.kitchenmanager.domain.model.dish;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public class DishCalculatorService {
    
    private final DishRepository dishRepo;
    
    public DishCalculatorService(DishRepository dishRepo) {
        this.dishRepo = dishRepo;
    }
    
    private BigDecimal summarize(List<Dish> dishes) {
        return dishes.stream().map(x -> x.getTotalPrice()).reduce(BigDecimal.ZERO, BigDecimal::add);
    }
    
    public float calculateDaily(LocalDate date) {
        return summarize(dishRepo.getDishesByDay(date)).floatValue();
    }
    
    public float calculateDailyPerDish(LocalDate date, MenuDish dish) {
        return summarize(dishRepo.getDishesByDayAndDish(date, dish)).floatValue();
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.axisdata.kitchenmanager.domain.model.dish;

import java.time.LocalDateTime;
import net.axisdata.kitchenmanager.domain.model.DomainEvent;
import net.axisdata.kitchenmanager.domain.model.dish.Dish;

public final class DishCooked implements DomainEvent {

    private final LocalDateTime occurredOn;
    private final Dish dish;

    public DishCooked(LocalDateTime occurredOn, Dish dish) {
        this.occurredOn = occurredOn;
        this.dish = dish;
    }

    @Override
    public LocalDateTime occurredOn() {
        return this.occurredOn;
    }

    public Dish getDish() {
        return dish;
    }

    @Override
    public String toString() {
        return "DishCooked{" + "occurredOn=" + occurredOn + ", dish=" + dish + '}';
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.axisdata.kitchenmanager.domain.model;

import java.util.ArrayList;
import java.util.List;

public class DomainEventPublisher {

  @SuppressWarnings("unchecked")
  private static final ThreadLocal<List> subscribers = new ThreadLocal<List>();

  private static final ThreadLocal<Boolean> publishing =
      new ThreadLocal<Boolean>() {
        protected Boolean initialValue() {
          return Boolean.FALSE;
        }
      };

  public static DomainEventPublisher instance() {
    return new DomainEventPublisher();
  }

  public DomainEventPublisher() {
    super();
  }

  @SuppressWarnings("unchecked")
  public <T> void publish(final T aDomainEvent) {
    if (publishing.get()) {
      return;
    }
    try {
      publishing.set(Boolean.TRUE);
      List<DomainEventSubscriber<T>> registeredSubscribers = subscribers.get();
      if (registeredSubscribers != null) {
        Class<?> eventType = aDomainEvent.getClass();
        for (DomainEventSubscriber<T> subscriber : registeredSubscribers) {
          Class<?> subscribedTo = subscriber.subscribedToEventType();
          if (subscribedTo == eventType || subscribedTo == DomainEvent.class) {
            subscriber.handleEvent(aDomainEvent);
          }
        }
      }
    } finally {
      publishing.set(Boolean.FALSE);
    }
  }

  public DomainEventPublisher reset() {
    if (!publishing.get()) {
      subscribers.set(null);
    }
    return this;
  }

  @SuppressWarnings("unchecked")
  public <T> void subscribe(DomainEventSubscriber<T> aSubscriber) {
    if (publishing.get()) {
      return;
    }
    List<DomainEventSubscriber<T>> registeredSubscribers = subscribers.get();
    if (registeredSubscribers == null) {
      registeredSubscribers = new ArrayList<DomainEventSubscriber<T>>();
      subscribers.set(registeredSubscribers);
    }
    registeredSubscribers.add(aSubscriber);
  }
}

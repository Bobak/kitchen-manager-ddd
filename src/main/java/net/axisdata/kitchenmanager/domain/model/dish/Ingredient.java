/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.axisdata.kitchenmanager.domain.model.dish;

import java.math.BigDecimal;
import java.util.Objects;
import net.axisdata.kitchenmanager.domain.model.ValueObject;

public class Ingredient implements ValueObject {

    private final String name;
    private final Double qunatity;
    private final Unit unit;
    private final BigDecimal pricePerUsedUnit;

    public Ingredient(String name, Double qunatity, Unit unit, BigDecimal pricePerUsedUnit) {
        this.name = name;
        this.qunatity = qunatity;
        this.unit = unit;
        this.pricePerUsedUnit = pricePerUsedUnit;
    }

    public String getName() {
        return name;
    }

    public Double getQunatity() {
        return qunatity;
    }

    public Unit getUnit() {
        return unit;
    }

    public BigDecimal getPricePerUsedUnit() {
        return pricePerUsedUnit;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + Objects.hashCode(this.name);
        hash = 41 * hash + Objects.hashCode(this.qunatity);
        hash = 41 * hash + Objects.hashCode(this.unit);
        hash = 41 * hash + Objects.hashCode(this.pricePerUsedUnit);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Ingredient other = (Ingredient) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.qunatity, other.qunatity)) {
            return false;
        }
        if (this.unit != other.unit) {
            return false;
        }
        if (!Objects.equals(this.pricePerUsedUnit, other.pricePerUsedUnit)) {
            return false;
        }
        return true;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.axisdata.kitchenmanager.domain.model;

import java.time.LocalDateTime;

public interface DomainEvent extends DomainObject {

  public LocalDateTime occurredOn();
  
}

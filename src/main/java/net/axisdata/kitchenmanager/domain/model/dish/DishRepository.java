/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.axisdata.kitchenmanager.domain.model.dish;

import java.time.LocalDate;
import java.util.List;
import net.axisdata.kitchenmanager.domain.model.Repository;

public interface DishRepository extends Repository {

    public void addDish(Dish dish);

    public void updateDish(Dish dish);

    public Dish getDish(String dishId);

    public List<Dish> getDishesByDay(LocalDate day);

    public List<Dish> getDishesByDayAndDish(LocalDate day, MenuDish dish);

    public String nextIdentity();

}

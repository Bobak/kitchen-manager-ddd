/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.axisdata.kitchenmanager.domain.model.dish;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import net.axisdata.kitchenmanager.domain.model.Aggregate;
import net.axisdata.kitchenmanager.domain.model.DomainEventPublisher;

public class Dish implements Aggregate {

    private String id;
    private MenuDish dish;
    private List<Ingredient> ingredients;
    private BigDecimal costOfIngredients;
    private BigDecimal totalPrice;
    private LocalDate makedAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public MenuDish getDish() {
        return dish;
    }

    public void setDish(MenuDish dish) {
        this.dish = dish;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public BigDecimal getCostOfIngredients() {
        return costOfIngredients;
    }

    public void setCostOfIngredients(BigDecimal costOfIngredients) {
        this.costOfIngredients = costOfIngredients;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public LocalDate getMakedAt() {
        return makedAt;
    }

    public void setMakedAt(LocalDate makedAt) {
        this.makedAt = makedAt;
    }

    public BigDecimal calculateCostOfIngrdients() {
        return ingredients.stream().map(x -> x.getPricePerUsedUnit().multiply(new BigDecimal(x.getQunatity()))).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public void finishCooking() {
        DomainEventPublisher.instance().publish(new DishCooked(LocalDateTime.now(), this));
    }

    public void acceptCooking(MenuDish dish) {
        DomainEventPublisher.instance().publish(new DishCookingAccepted(LocalDateTime.now(), dish));
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", dishName=" + dish + ", ingredients=" + ingredients + ", costOfIngredients=" + costOfIngredients + ", totalPrice=" + totalPrice + ", makedAt=" + makedAt + '}';
    }

}

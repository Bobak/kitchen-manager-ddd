/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.axisdata.kitchenmanager.domain.model.dish;

import java.math.BigDecimal;

public enum MenuDish {

    TORTILLA_DA_PATATAS("Tortilla da Patatas", new BigDecimal(12.0), 10000L),
    MOSKOL("Moskol", new BigDecimal(8.0), 1000L);

    private final String name;
    private final BigDecimal price;
    private final Long cookingTime;

    private MenuDish(String name, BigDecimal price, Long preparingTime) {
        this.name = name;
        this.price = price;
        this.cookingTime = preparingTime;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Long getCookingTime() {
        return cookingTime;
    }

}

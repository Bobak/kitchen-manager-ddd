/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.axisdata.kitchenmanager.domain.model.dish;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import net.axisdata.kitchenmanager.domain.model.DomainFactory;

public class DishFactory implements DomainFactory {

    public static Dish makeDish(Dish dish) {

        switch (dish.getDish()) {
            case MOSKOL:
                return makeMoskol(dish);
            case TORTILLA_DA_PATATAS:
                return makeTortillaDePatatas(dish);
            default:
                throw new IllegalStateException("We do not have that dish in our Menu");
        }

    }

    private static Dish makeTortillaDePatatas(Dish dish) {

        List ingredients = new LinkedList<Ingredient>();

        Ingredient potato = new Ingredient("potato", 4.0, Unit.PCS, new BigDecimal(0.25));
        Ingredient onion = new Ingredient("onion", 1.0, Unit.PCS, new BigDecimal(0.10));
        Ingredient egg = new Ingredient("egg", 6.0, Unit.PCS, new BigDecimal(0.20));
        Ingredient salt = new Ingredient("salt", 0.5, Unit.SPOON, new BigDecimal(0.10));
        Ingredient oil = new Ingredient("oil", 300.0, Unit.ML, new BigDecimal(0.01));

        ingredients.add(potato);
        ingredients.add(onion);
        ingredients.add(egg);
        ingredients.add(salt);
        ingredients.add(oil);

        dish.setIngredients(ingredients);
        dish.setCostOfIngredients(dish.calculateCostOfIngrdients());
        dish.setTotalPrice(MenuDish.TORTILLA_DA_PATATAS.getPrice());
        dish.setMakedAt(LocalDate.now());

        try {
            Thread.sleep(MenuDish.TORTILLA_DA_PATATAS.getCookingTime());
        } catch (InterruptedException ex) {
            throw new IllegalStateException("Dish cannot be preapred becuase kitchen blow up!!!");
        }

        return dish;
    }

    private static Dish makeMoskol(Dish dish) {

        List ingredients = new LinkedList<Ingredient>();

        Ingredient potato = new Ingredient("potato", 4.0, Unit.PCS, new BigDecimal(0.25));
        Ingredient onion = new Ingredient("flour", 0.1, Unit.KG, new BigDecimal(0.10));
        Ingredient egg = new Ingredient("egg", 1.0, Unit.PCS, new BigDecimal(0.20));
        Ingredient salt = new Ingredient("milk", 250.0, Unit.ML, new BigDecimal(0.0025));
        Ingredient oil = new Ingredient("oil", 300.0, Unit.ML, new BigDecimal(0.01));

        ingredients.add(potato);
        ingredients.add(onion);
        ingredients.add(egg);
        ingredients.add(salt);
        ingredients.add(oil);

        dish.setIngredients(ingredients);
        dish.setCostOfIngredients(dish.calculateCostOfIngrdients());
        dish.setTotalPrice(MenuDish.MOSKOL.getPrice());
        dish.setMakedAt(LocalDate.now());

        try {
            Thread.sleep(MenuDish.MOSKOL.getCookingTime());
        } catch (InterruptedException ex) {
            throw new IllegalStateException("Dish cannot be preapred becuase kitchen blow up!!!");
        }

        return dish;
    }

}

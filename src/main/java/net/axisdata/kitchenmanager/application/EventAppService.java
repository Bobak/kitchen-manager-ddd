/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.axisdata.kitchenmanager.application;

import net.axisdata.kitchenmanager.domain.model.DomainEvent;
import net.axisdata.kitchenmanager.domain.model.DomainEventPublisher;
import net.axisdata.kitchenmanager.domain.model.DomainEventSubscriber;

public class EventAppService {

    public EventAppService() {
        listen();
    }

    private static void listen() {
        DomainEventPublisher.instance().reset();

        DomainEventPublisher.instance()
                .subscribe(new DomainEventSubscriber<DomainEvent>() {

                    public void handleEvent(DomainEvent aDomainEvent) {
                       
                    }

                    public Class<DomainEvent> subscribedToEventType() {
                        return DomainEvent.class;
                    }
                });
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.axisdata.kitchenmanager.application;

import net.axisdata.kitchenmanager.domain.model.DomainEventPublisher;
import net.axisdata.kitchenmanager.domain.model.DomainEventSubscriber;
import net.axisdata.kitchenmanager.domain.model.dish.Dish;
import net.axisdata.kitchenmanager.domain.model.dish.DishCooked;
import net.axisdata.kitchenmanager.domain.model.dish.DishCookingAccepted;
import net.axisdata.kitchenmanager.domain.model.dish.DishFactory;
import net.axisdata.kitchenmanager.domain.model.dish.DishRepository;
import net.axisdata.kitchenmanager.domain.model.dish.MenuDish;

public class KitchenAppService {

    private final DishRepository dishRepository;

    public KitchenAppService(DishRepository dishRepository) {
        this.dishRepository = dishRepository;
        dishAcceptedCookingHandler();
        dishCookedHandler();
    }

    public void makeDish(MenuDish dishOrdered) throws InterruptedException {

        //Make some validation here in order to be sure you gonna be able make that Dish (enough ingredients in ingredients inventory (fridge) etc.) 
        Dish newDish = new Dish();
        newDish.setId(dishRepository.nextIdentity());
        newDish.acceptCooking(dishOrdered);
        newDish.setDish(dishOrdered);
        newDish = DishFactory.makeDish(newDish);
        dishRepository.addDish(newDish);
        newDish.finishCooking();
    }

    private void dishAcceptedCookingHandler() {

        DomainEventSubscriber subscriber
                = new DomainEventSubscriber<DishCookingAccepted>() {
            @Override
            public void handleEvent(DishCookingAccepted aDomainEvent) {

                System.out.println(aDomainEvent.getDish().getName() + " has been accepted to cook at: " + aDomainEvent.occurredOn()
                );
            }

            @Override
            public Class<DishCookingAccepted> subscribedToEventType() {
                return DishCookingAccepted.class;
            }
        };
        DomainEventPublisher.instance().subscribe(subscriber);
    }

    private void dishCookedHandler() {

        DomainEventSubscriber subscriber
                = new DomainEventSubscriber<DishCooked>() {
            @Override
            public void handleEvent(DishCooked aDomainEvent) {

                System.out.println(aDomainEvent.getDish().getDish().getName() + " has been cooked at: " + aDomainEvent.occurredOn());
            }

            @Override
            public Class<DishCooked> subscribedToEventType() {
                return DishCooked.class;
            }
        };
        DomainEventPublisher.instance().subscribe(subscriber);
    }

}
